package com.example.springbootdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDockerApplication {
	@GetMapping("/greeting")
	public String home() {
		return "Hello Gitlab-CI Argo-CD World !! Capgemini";
	}

	@GetMapping("/weekend")
	public String weekend() {
		return "Happy weekend Gitlab-CI Argo-CD World";
	}
	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerApplication.class, args);
	}

}
